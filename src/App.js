import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./redux/store";
import Dashboard from "./layout/Dashboard";
import Login from "./layout/Login";
import Details from "./layout/Details";

function App() {
	return (
		<>
			<Router>
				<Provider store={store}>
					<Switch>
						<Route exact path="/" component={Dashboard} />
            <Route exact path="/login" component={Login} />
						<Route exact path="/details" component={Details} />
					</Switch>
				</Provider>
			</Router>
		</>
	);
}

export default App;
