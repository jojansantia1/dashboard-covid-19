import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { obtenerPaisesAction } from "../redux/actions/consultasAction";
import Data from "../components/Data";
import Paginacion from "../utils/Paginacion";
import Swal from "sweetalert2";

const Container = () => {
	const dispatch = useDispatch();
	const paises = useSelector((state) => state.infoApi.paises);
	const error = useSelector((state) => state.infoApi.error);
	const pais = useSelector((state) => state.infoApi.pais);

	if (error) {
		Swal.fire({
			icon: "error",
			title: "There was an error",
			text: "An error occurred while querying the api, please try again.",
		});
	}

	useEffect(() => {
		const cargarPaises = () => {
			dispatch(obtenerPaisesAction());
		};
		cargarPaises();
		// eslint-disable-next-line
	}, []);

	// Paginacion
	const [currentPage, setCurrentPage] = useState(1);
	const [postsPerPage] = useState(15);

	// Get current posts
	const indexOfLastPost = currentPage * postsPerPage;
	const indexOfFirstPost = indexOfLastPost - postsPerPage;
	const currentPosts =
		pais.length !== 0 ? pais : paises.slice(indexOfFirstPost, indexOfLastPost);

	// Change page
	const paginate = (pageNumber) => setCurrentPage(pageNumber);

	return (
		<div className="col-10 mx-auto mb-4">
			<div className="table-responsive my-4">
				<table className="table table-hover ">
					<thead className="thead-light bg-dark text-white">
						<tr>
							<th scope="col " className="text-center align-middle">
								Country
							</th>
							<th scope="col " className="text-center align-middle">
								Total cases
							</th>
							<th scope="col " className="text-center align-middle">
								Deaths
							</th>
							<th scope="col " className="text-center align-middle">
								Recovered
							</th>
							<th scope="col " className="text-center align-middle">
								Active cases
							</th>
							<th scope="col " className="text-center align-middle">
								Acciones
							</th>
						</tr>
					</thead>
					<tbody>
						{currentPosts.map((data) => {
							return <Data key={data.Country_text} data={data} />;
						})}
					</tbody>
				</table>
			</div>
			<Paginacion
				postsPerPage={postsPerPage}
				totalPosts={pais ? pais.length : paises.length}
				paginate={paginate}
				currentPage={currentPage}
			/>
		</div>
	);
};

export default Container;
