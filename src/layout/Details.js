import React from "react";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

const Details = () => {
	const history = useHistory();
	const pais = useSelector((state) => state.infoApi.paisSeleccionado);
	let currentUser = localStorage.getItem("User");

	if (!currentUser) {
		history.push("/login");
	}

	if(!pais?.Country_text){
		history.push("/");
	}

	const handleReturn = () => {
		history.push("/");
	};

	return (
		<div className="login">
			<div className="card col-10 col-sm-8 container-fluid my-auto ">
				<div class="card-header text-center col-12">
					Details Country Covid-19
				</div>
				<div className="card-body">
					<h1 className="card-title text-center mb-4">{pais.Country_text}</h1>
					<span class="list-group-item d-flex justify-content-between  ">
						<h5> Total Cases</h5>
						<p>{pais["Total Cases_text"] || 0}</p>
					</span>
					<span class="list-group-item d-flex justify-content-between  ">
						<h5>New Cases</h5>
						<p className="font-weight-bold text-danger">
							{pais["New Cases_text"] || 0}
						</p>
					</span>
					<span class="list-group-item d-flex justify-content-between ">
						<h5>Total Deaths</h5>
						<p>{pais["Total Deaths_text"] || 0}</p>
					</span>
					<span class="list-group-item d-flex justify-content-between  ">
						<h5>New Deaths</h5>
						<p className="font-weight-bold text-warning">
							{pais["New Deaths_text"] || 0}
						</p>
					</span>
					<span class="list-group-item d-flex justify-content-between ">
						<h5>Total Recovered</h5>
						<p className="font-weight-bold text-success">
							{pais["Total Recovered_text"] || 0}
						</p>
					</span>
					<span class="list-group-item d-flex justify-content-between ">
						<h5>Active Cases</h5>
						<p>{pais["Active Cases_text"] || 0}</p>
					</span>
					<div class="text-center mt-4">
						<h5>Last Update</h5>
						<p>{pais["Last Update"] || 0}</p>
					</div>
					<button
						onClick={handleReturn}
						type="button"
						class="btn btn-lg btn-primary"
					>
						Return
					</button>
				</div>
			</div>
		</div>
	);
};

export default Details;
