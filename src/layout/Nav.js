import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getUserAction } from "../redux/actions/userAction";
import FormSearch from "../components/FormSearch";
import imgDefault from "../assets/imgDefault.png";
import { useHistory } from "react-router-dom";
import Swal from "sweetalert2";

const Nav = ({ change, clss }) => {
	const history = useHistory();
	const dispatch = useDispatch();
	const user = useSelector((state) => state.users.user);
	const error = useSelector((state) => state.users.error);
	let currentUser = localStorage.getItem("User");

	if (error) {
		Swal.fire({
			icon: "error",
			title: "There was an error",
			text: "An error occurred while querying the database, please try again.",
		});
	}

	useEffect(() => {
		if (currentUser) dispatch(getUserAction(currentUser));
		// eslint-disable-next-line
	}, []);

	if (!currentUser) {
		history.push("/login");
	}

	return (
		<nav className="navbar navbar-light bg-light justify-content-between navPpal ">
			<div className="mx-2 row col-7 col-md-4">
				<div className="col-3 btnNav">
					<button
						onClick={() => change(clss)}
						className="navbar-toggler btnToggle "
						type="button"
					>
						<span className="navbar-toggler-icon"></span>
					</button>
				</div>
				<FormSearch />
			</div>

			<img
				className="rounded-circle mx-2 imgDefault"
				src={user?.picture?.thumbnail || imgDefault}
				alt="user profile"
			/>
		</nav>
	);
};

export default Nav;
