import React from 'react'

const Footer = () => {
  return (
    <div className="d-flex justify-content-between mx-auto bg-secondary text-white">
      <p className="pt-2 px-4">Jojansantia &copy;</p>
      <p className="pt-2 px-4">Dashboard Covid-19</p>
    </div>
  )
}

export default Footer
