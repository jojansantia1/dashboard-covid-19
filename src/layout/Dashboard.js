import React, { useState } from "react";
import Container from "./Container";
import Footer from "./Footer";
import Nav from "./Nav";
import DivCards from "../components/DivCards";

const Dashboard = () => {
	const [clss, setClss] = useState(true);

	const change = (clss) => {
		let elm = document.getElementById("navbarToggleExternalContent");
		if (!clss) {
			elm.classList.add("menuLatNone");
		} else {
			elm.classList.remove("menuLatNone");
		}
		setClss(!clss);
	};

	return (
		<div>
			<div className=" divPpal row  ">
				<div
					className="col-6 col-sm-4 col-md-3  col-xl-2 menuLat menuLatNone "
					id="navbarToggleExternalContent"
				>
					<div className="menu bg-dark py-4 ps-5  col-sm-4 col-md-3 col-5 col-xl-2">
						<svg
							onClick={() => change(false)}
							className=""
							xmlns="http://www.w3.org/2000/svg"
							width="24"
							height="24"
							fill="#fff"
							className="bi bi-x svgIcon"
							viewBox="0 0 16 16"
							style={{ right: "35px", position: "absolute", cursor: "pointer" }}
						>
							<path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
						</svg>
						<ul className="list-group d-grid gap-5">
							<li>Item One</li>
							<li>Item Two</li>
							<li>Item Three</li>
							<li>Item Four</li>
							<li>Item Five</li>
						</ul>
					</div>
				</div>

				<div className=" col-xl-10 col-md-9 col-xs-12 divInfo">
					<Nav change={change} clss={clss} />
					<Container />
					<DivCards />
					<Footer />
				</div>
			</div>
		</div>
	);
};

export default Dashboard;
