import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { getUserAction } from "../redux/actions/userAction";

const Login = () => {
	const history = useHistory();
	const dispatch = useDispatch();
	const [alert, setAlert] = useState("");
	const [sesion, setSesion] = useState({
		username: "",
		password: "",
	});
	const { username, password } = sesion;

	let currentUser = localStorage.getItem("User");
	if (currentUser) {
		history.push("/");
	}

	const handleSesion = (e) => {
		setSesion({
			...sesion,
			[e.target.name]: e.target.value,
		});
	};

	const handleButton = () => {
		if (username.trim() === "") {
			setAlert("Username cannot be empty.");
			setTimeout(() => {
				setAlert("");
			}, 3000);
			return;
		}
		if (password.trim() === "") {
			setAlert("Password cannot be empty.");
			setTimeout(() => {
				setAlert("");
			}, 3000);
			return;
		}
		dispatch(getUserAction(username, password));
		setAlert("Validando...");
		setTimeout(() => {
			setAlert("");
			history.push("/");
		}, 1000);
	};

	return (
		<div className="login">
			<div className="container-fluid col-xl-4 col-lg-6 col-10 col-sm-8 my-auto align-middle">
				{alert && (
					<p class="alert alert-warning text-center" role="alert">
						{alert}
					</p>
				)}
				<h1 className="text-center">Sign in to Dashboard</h1>
				<div className="card p-3 ">
					<div className="mb-3">
						<label htmlFor="username" className="form-label">
							Username
						</label>
						<div className="">
							<input
								className="form-control"
								id="username"
								name="username"
								type="text"
								placeholder="Enter a username"
								onChange={(e) => handleSesion(e)}
								value={username}
							/>
						</div>
					</div>
					<div className="mb-3">
						<label htmlFor="password" className="form-label">
							Password
						</label>
						<div className="">
							<input
								className="form-control "
								name="password"
								id="password"
								type="password"
								placeholder="Enter a password"
								onChange={(e) => handleSesion(e)}
								value={password}
							/>
						</div>
					</div>
					<div className="text-center w-full">
						<button
							onClick={handleButton}
							id="button"
							className="btn col-6 btn-primary"
						>
							Sign in
						</button>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Login;
