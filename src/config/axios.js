import axios from "axios";

// end point base API
const clienteAxios = axios.create({
	baseURL:"https://covid-19.dataflowkit.com/"
});

export default clienteAxios;