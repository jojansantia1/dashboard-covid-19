import React, { useEffect } from "react";

const Paginacion = ({ currentPage, postsPerPage, totalPosts, paginate }) => {
	const pageNumbers = [];
	const maxPage = Math.ceil(totalPosts / postsPerPage);

	for (let i = 1; i <= maxPage; i++) {
		pageNumbers.push(i);
	}

	useEffect(() => {
		paginate(1) //Se devuelve a la pag 1 automaticamente
		//eslint-disable-next-line
	}, [totalPosts])


	return (
		<div className="container">
      <nav className="d-flex justify-content-center">
        <ul className="pagination">
        {currentPage !== 1 && (<>
          <li className="page-item ">
            <a className="page-link" href="#"  onClick={() => paginate(currentPage - 1)}>Previous</a>
          </li>
          <li className="page-item"><a className="page-link" href="#" onClick={() => paginate(currentPage - 1)}> {currentPage - 1}</a></li>
       </> )}
          <li className="page-item active" aria-current="page">
            <a className="page-link" href="#" onClick={() => paginate(currentPage)}>{currentPage}</a>
          </li>
        {currentPage !== maxPage && (<>
          <li className="page-item"><a className="page-link" href="#" onClick={() => paginate(currentPage + 1)}>{currentPage + 1}</a></li>
          <li className="page-item">
            <a className="page-link" href="#" onClick={() => paginate(currentPage + 1)}>Next</a>
          </li>
        </>)}
        </ul>
      </nav>
		</div>
	);
};

export default Paginacion;
