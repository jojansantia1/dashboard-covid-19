import React from "react";
import { useDispatch } from "react-redux";
import { detallesPaisAction } from "../redux/actions/consultasAction";
import { useHistory } from "react-router-dom";

const Data = ({ data }) => {
	const history = useHistory();
	const dispatch = useDispatch();

	const handleDetails = () => {
		dispatch(detallesPaisAction(data.Country_text));
		setTimeout(() => {
			history.push("/details");
		}, 300);
	};

	let rowClass = "text-center align-middle";

	return (
		<tr>
			<th scope="row" className={rowClass}>
				{data.Country_text}
			</th>
			<td className={rowClass}>{data["Total Cases_text"]}</td>
			<td className={rowClass}>{data["Total Deaths_text"]}</td>
			<td className={rowClass}>{data["Total Recovered_text"]}</td>
			<td className={rowClass}>{data["Active Cases_text"]}</td>
			<td className={rowClass}>
				<button type="button" className="btn btn-link" onClick={handleDetails}>
					Details
				</button>
			</td>
		</tr>
	);
};

export default Data;
