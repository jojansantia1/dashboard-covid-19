import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { obtenerDatosPaisAction } from "../redux/actions/consultasAction";
import Swal from "sweetalert2";

const FormSearch = () => {
	const [search, setSearch] = useState("");
	const dispatch = useDispatch();
	const sendSearch = (e) => {
		e.preventDefault();
		if (search.trim() === "") {
			Swal.fire({
				icon: "error",
				title: "There was an error",
				text: "Please enter an country.",
			});
			return;
		}
		dispatch(obtenerDatosPaisAction(search));
	};

	return (
		<form
			onSubmit={sendSearch}
			className="form-inline d-flex col-9 col-sm-7 col-md-12"
		>
			<input
				className="form-control mx-2 col-12"
				type="search"
				name="search"
				placeholder="Search for country..."
				aria-label="Search"
				value={search}
				onChange={(e) => setSearch(e.target.value)}
			/>
			<button type="submit" className="btn btn-primary">
				Search
			</button>
		</form>
	);
};

export default FormSearch;
