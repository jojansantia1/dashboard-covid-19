import React from "react";

const DivCards = () => {
	return (
		<div className="row col-11 mx-auto mb-5">
			<div className="row mb-2 mx-auto">
				<div className="col-12 mx-auto">
          <div className="bg-light div1 text-secondary d-flex justify-content-between p-3 ">
					<p>Hello user</p>
					<p>Recent items</p>
				</div>
        </div>
			</div>
			<div className="row mb-2 mx-auto">
				<div className="col-12 col-xl-4 col-sm-6 col-xxl-3">
					<div className="bg-secondary  d-flex justify-content-between p-3 border border-white text-white align-middle">
						<p className="pt-2 px-2">Overview</p>
						<p className="pt-2 px-2">Card</p>
					</div>
				</div>
				<div className="col-12 col-xl-4 col-sm-6 col-xxl-3">
					<div className="bg-secondary  d-flex justify-content-between p-3 border border-white text-white align-middle">
						<p className="pt-2 px-2">Overview</p>
						<p className="pt-2 px-2">Card</p>
					</div>
				</div>
				<div className="col-12 col-xl-4 col-sm-6 col-xxl-3">
					<div className="bg-secondary  d-flex justify-content-between p-3 border border-white text-white align-middle">
						<p className="pt-2 px-2">Overview</p>
						<p className="pt-2 px-2">Card</p>
					</div>
				</div>
				<div className="col-12 col-xl-4 col-sm-6 col-xxl-3">
					<div className="bg-secondary  d-flex justify-content-between p-3 border border-white text-white align-middle">
						<p className="pt-2 px-2">Overview</p>
						<p className="pt-2 px-2">Card</p>
					</div>
				</div>
			</div>
			<div className="row mb-2 mx-auto">
				<div className="col-12 col-md-6 text-center mb-2">
					<div className="div2 bg-info col-12 border border-white text-white pt-3">
						Card
					</div>
				</div>
				<div className="col-12 col-md-6 border border-white text-white text-center">
					<div className="div3 bg-info border border-white mb-2 pt-3">
						<p>Card</p>
					</div>
					<div className="div4  bg-info border border-white pt-3">
						<p>Card</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default DivCards;
