import { combineReducers } from "redux";
import consultasReducer from "./consultasReducer";
import userReducer from "./userReducer";

export default combineReducers({
	infoApi: consultasReducer,
	users: userReducer,
});
