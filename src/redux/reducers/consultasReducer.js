import {
	OBTENER_PAISES,
	OBTENER_DATOS_PAIS,
	ERROR_CARGA,
	COMENZAR_DESCARGA,
	PAIS_SELECCIONADO,
	ERROR_CARGA_PAISES
} from "../types";

const initialState = {
	paises: [],
	pais: [],
	paisSeleccionado: [],
	error: null,
};

export default function (state = initialState, action) {
	switch (action.type) {
		case COMENZAR_DESCARGA:
			return {
				...state,
				error: false,
			};
		case OBTENER_PAISES:
			return {
				...state,
				paises: action.payload,
			};
		case OBTENER_DATOS_PAIS:
			return {
				...state,
				pais: [action.payload],
			};
		case ERROR_CARGA:
			return {
				...state,
				error: true,
				pais: [],
			};
		case ERROR_CARGA_PAISES:
			return {
				...state,
				error: true,
				paises: [],
			};
		case PAIS_SELECCIONADO:
			return {
				...state,
				paisSeleccionado: action.payload,
			};
		default:
			return state;
	}
}
