import {  GET_USER, COMENZAR_DESCARGA_USER, ERROR_CARGA_USUARIO } from "../types";

const initialState = {
	user: null,
	error: false
};

export default function (state = initialState, action) {
	switch (action.type) {
		case COMENZAR_DESCARGA_USER:
			return {
				...state,
				error: false,
			};
    case GET_USER:
			return {
				...state,
				user: action.payload,
			};
		case ERROR_CARGA_USUARIO:
			return {
				...state,
				error: true,
				user: null,
			};
		default:
			return state;
	}
}
