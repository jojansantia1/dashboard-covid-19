import {
	GET_USER,
	COMENZAR_DESCARGA_USER,
	ERROR_CARGA_USUARIO,
} from "../types";
import axios from "axios";
import Swal from "sweetalert2";

export function getUserAction(username, password) {
	return async (dispatch) => {
		dispatch(comenzarDescarga());
		try {
			const respuesta = await axios.get(
				`http://localhost:4000/users/${username}`
			);
			if (password) {
				let passUser = respuesta.data.login.password;
				if (password === passUser) {
					localStorage.setItem("User", username);
				} else {
					Swal.fire({
						icon: "error",
						title: "There was an error",
						text: "Passwords do not match, please try again.",
					});
					return;
				}
			}
			dispatch(descargaExitosa(respuesta.data));
		} catch (error) {
			Swal.fire({
				icon: "error",
				title: "There was an error",
				text: "Database not found, please try again.",
			});
			dispatch(descargaUsuarioError());
		}
	};
}

const comenzarDescarga = () => ({
	type: COMENZAR_DESCARGA_USER,
});

const descargaExitosa = (user) => ({
	type: GET_USER,
	payload: user,
});

const descargaUsuarioError = () => ({
	type: ERROR_CARGA_USUARIO,
});
