import {
	OBTENER_PAISES,
	OBTENER_DATOS_PAIS,
	ERROR_CARGA,
	COMENZAR_DESCARGA,
	PAIS_SELECCIONADO,
	ERROR_CARGA_PAISES,
} from "../types";
import clienteAxios from "../../config/axios";
import Swal from "sweetalert2";

export function obtenerPaisesAction() {
	return async (dispatch) => {
		dispatch(comenzarDescarga());
		try {
			const respuesta = await clienteAxios.get("/v1");
			dispatch(descargaExitosa(respuesta.data));
		} catch (error) {
			dispatch(descargaPaisesError());
		}
	};
}

const descargaExitosa = (paises) => ({
	type: OBTENER_PAISES,
	payload: paises,
});

const descargaPaisesError = () => ({
	type: ERROR_CARGA_PAISES,
});

export function obtenerDatosPaisAction(pais) {
	return async (dispatch) => {
		dispatch(comenzarDescarga());
		try {
			const respuesta = await clienteAxios.get(`/v1/${pais}`);
			let resPais = respuesta.data.Country_text.toLowerCase();
			if (pais.toLowerCase() === resPais) {
				dispatch(descargaPaisExitosa(respuesta.data));
			} else {
				dispatch(descargaPaisError());
				Swal.fire({
					icon: "error",
					title: "There was an error",
					text: "The country entered was not found, please try again.",
				});
			}
		} catch (error) {
			dispatch(descargaPaisError());
		}
	};
}

const comenzarDescarga = () => ({
	type: COMENZAR_DESCARGA,
});

const descargaPaisExitosa = (pais) => ({
	type: OBTENER_DATOS_PAIS,
	payload: pais,
});

const descargaPaisError = () => ({
	type: ERROR_CARGA,
});

export function detallesPaisAction(pais) {
	return async (dispatch) => {
		try {
			const respuesta = await clienteAxios.get(`/v1/${pais}`);
			dispatch(descargaPaisSeleccionado(respuesta.data));
		} catch (error) {}
	};
}

const descargaPaisSeleccionado = (pais) => ({
	type: PAIS_SELECCIONADO,
	payload: pais,
});
